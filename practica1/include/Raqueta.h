// Raqueta.h: interface for the Raqueta class.
//
//////////////////////////////////////////////////////////////////////

#include "Plano.h"
#include "Vector2D.h"

class Raqueta : public Plano  
{
public:
	

	Raqueta();
	virtual ~Raqueta();

	Vector2D velocidad;


	void Mueve(float t);
};
